# Open files in the default editor

You can find what editor is assigned to a given file extension, and tell the editor to open files or folders.

Implementations for:

 * macOS: uses `CoreServices.framework`, and has special-case workarounds for quirks of Xcode and Sublime Text 3.

 * Unix: uses `xdg-mime` + `gtk-launch` and `xdg-open` as a fallback.

 * Windows: not supported yet.
