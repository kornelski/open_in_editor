use std::borrow::Cow;
use std::path::Path;
use std::sync::Arc;
use crate::Editor;
use crate::Error;
use crate::EditorImpl;
use std::process::Command;

/// Reveal paths in the system default file manager
///
/// This call is non-blocking
pub fn reveal_paths(paths: &[&Path]) -> Result<(), Error> {
    let paths: Vec<&Path> = paths.iter().copied().filter_map(|p| {
        if !p.is_dir() {
            p.parent()
        } else {
            Some(p)
        }
    }).collect();

    open_paths(&paths)
}

fn launch(cmd: &mut Command) -> Result<(), Error> {
    match cmd.status() {
        Ok(s) if s.success() => Ok(()),
        Ok(s) => Err(Error::Status(s.code().unwrap_or(-1))),
        Err(e) => Err(Error::IO(e.kind())),
    }
}

fn output(cmd: &mut Command) -> Option<Box<str>> {
    let out = cmd.output().ok()?;
    if !out.status.success() {
        return None;
    }
    let out = String::from_utf8(out.stdout).ok()?;
    let out = out.trim();
    if out.is_empty() {
        return None;
    }
    Some(out.into())
}

pub fn open_paths(paths: &[&Path]) -> Result<(), Error> {
    match launch(Command::new("xdg-open").args(paths)) {
        Ok(()) => return Ok(()),
        err => {
            if launch(Command::new("exo-open").args(paths)).is_ok() {
                return Ok(());
            }
            if launch(Command::new("kde-open").args(paths)).is_ok() {
                return Ok(());
            }
            if paths.len() == 1 {
                if launch(Command::new("kfmclient").arg("openURL").args(path[0])).is_ok() {
                    return Ok(());
                }
                if launch(Command::new("kioclient").arg("exec").args(path[0])).is_ok() {
                    return Ok(());
                }
            }
            err
        },
    }
}

pub fn editor_for_file_extension(ext: &str) -> Option<Editor> {
    let dir = tempfile::tempdir().ok()?;
    let path = dir.path().join(format!("{}.{}", ext, ext));
    std::fs::write(&path, "").ok()?;

    let mime = output(Command::new("xdg-mime").arg("query").arg("filetype").arg(path))?;
    let desktop_filename = output(Command::new("xdg-mime").arg("query").arg("default").arg(&*mime))?;
    Some(Editor {
        inner: Arc::new(XdgDesktop { desktop_filename }),
    })
}

struct XdgDesktop {
    desktop_filename: Box<str>,
}

impl XdgDesktop {
    fn get_desktop_file(&self) -> Option<String> {
        let home = home::home_dir();
        let home = home.as_deref().unwrap_or(Path::new("/root"));
        let local = home.join(".local/share/applications/");

        let roots = [local.as_path(), Path::new("/usr/share/applications/"), Path::new("/usr/local/share/applications/")];
        for path in roots.iter() {
            if let Ok(file) = std::fs::read_to_string(path.join(&*self.desktop_filename)) {
                return Some(file);
            }
        }
        None
    }
}

impl EditorImpl for XdgDesktop {
    fn open_paths(&self, paths: &[&Path]) -> Result<(), Error> {
        // org.gnome.gedit.desktop requires files, not dirs :(
        match launch(Command::new("gtk-launch").arg(&*self.desktop_filename).args(paths)) {
            Ok(()) => Ok(()),
            err => {
                if launch(Command::new("kioclient").arg("exec").arg("--").arg(&*self.desktop_filename).args(paths)).is_ok() {
                    return Ok(());
                }
                // desperate fallback
                if open_paths(paths).is_ok() {
                    return Ok(());
                }
                err
            },
        }
    }

    fn name(&self) -> Cow<str> {
        if let Some(d) = self.get_desktop_file() {
            let name = d.lines().find(|l| l.starts_with("Name=")).map(|l| l[5..].trim_end());
            if let Some(name) = name {
                return name.to_string().into();
            }
        }
        self.desktop_filename.trim_end_matches(".desktop").into()
    }
}
