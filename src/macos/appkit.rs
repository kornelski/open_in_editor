use objc::runtime::Object;
use objc::class;
use objc::msg_send;
use objc::sel;
use objc::sel_impl;
use core_foundation::array::CFArrayRef;

#[link(name = "AppKit", kind = "framework")]
extern "C" {
}

pub fn reveal_paths(paths: CFArrayRef) -> bool {
    let cls = class!(NSWorkspace);
    unsafe {
        let ws: *mut Object = msg_send![cls, sharedWorkspace];
        if ws.is_null() {
            return false;
        }
        let _: () = msg_send![ws, activateFileViewerSelectingURLs:paths];
    }
    true
}
