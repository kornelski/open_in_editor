use core_foundation::array::CFArray;
use core_foundation::base::TCFType;
use core_foundation::base::TCFTypeRef;
use core_foundation::bundle::CFBundle;
use core_foundation::bundle::CFBundleRef;
use core_foundation::string::CFStringRef;
use core_foundation::url::CFURLRef;
use core_foundation::url::CFURL;
use core_services::*;

pub(crate) unsafe fn maybe_create<T: TCFType>(res: T::Ref) -> Option<T> {
    if !res.as_void_ptr().is_null() {
        Some(T::wrap_under_create_rule(res))
    } else {
        None
    }
}

pub(crate) fn uti_for_extension(ext: &str) -> Option<CFString> {
    let ext = CFString::new(ext);
    unsafe {
        maybe_create(UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, ext.as_concrete_TypeRef(), std::ptr::null()))
    }
}

pub(crate) fn default_handler_bundle_identifier_for_uti(uti: CFStringRef, roles: LSRolesMask) -> Option<CFString> {
    unsafe {
        maybe_create(LSCopyDefaultRoleHandlerForContentType(uti, roles))
    }
}

pub(crate) fn urls_for_bundle_identifier(bundle_id: CFStringRef) -> Vec<CFURL> {
    let arr = unsafe {
        maybe_create::<CFArray<CFURL>>(LSCopyApplicationURLsForBundleIdentifier(bundle_id, std::ptr::null_mut()))
    };
    match arr {
        Some(arr) => arr.iter().map(|url| url.clone()).collect(),
        None => Vec::new(),
    }
}

/// bundle + bundle id
pub fn cfbundle_for_file_extension(ext: &str) -> Option<(CFBundle, String)> {
    let uti = uti_for_extension(ext)?;
    let bundle_id = default_handler_bundle_identifier_for_uti(uti.as_concrete_TypeRef(), kLSRolesEditor | kLSRolesViewer)?;
    let urls = urls_for_bundle_identifier(bundle_id.as_concrete_TypeRef());
    urls.into_iter().find_map(CFBundle::new).map(|bundle| (bundle, bundle_id.to_string()))
}

extern "C" {
    pub fn CFBundleCopyBundleURL(bundle: CFBundleRef) -> CFURLRef;
}
