use core_foundation::array::CFArray;
use core_foundation::base::TCFType;
use core_foundation::bundle::CFBundle;
use core_foundation::url::CFURL;
use core_foundation::url::kCFURLPOSIXPathStyle;
use core_services::*;
use crate::Editor;
use crate::EditorImpl;
use crate::Error;
use self::services::*;
use std::borrow::Cow;
use std::path::Path;
use std::path::PathBuf;
use std::sync::Arc;
use std::sync::Mutex;

mod services;
mod appkit;

pub struct EditorBundle {
    bundle: Mutex<BundleIsSendable>,
}

struct BundleIsSendable(CFBundle);
unsafe impl Send for BundleIsSendable {}

impl EditorImpl for EditorBundle {
    fn name(&self) -> Cow<str> {
        let bundle = self.bundle.lock().unwrap();
        let info = bundle.0.info_dictionary();
        let name = info.get(CFString::new("CFBundleName"));
        name.downcast::<CFString>().expect("CFBundleName").to_string().into()
    }

    fn open_paths<'a>(&self, paths: &[&Path]) -> Result<(), Error> {
        self.open_cfurls(array_of_paths(paths)?)
    }
}

impl EditorBundle {
    fn new(bundle: CFBundle) -> Self {
        Self {
            bundle: Mutex::new(BundleIsSendable(bundle)),
        }
    }

    fn bundle_url(&self) -> Result<CFURL, Error> {
        let bundle = self.bundle.lock().unwrap();
        unsafe {
            maybe_create::<CFURL>(CFBundleCopyBundleURL(bundle.0.as_concrete_TypeRef()))
        }.ok_or(Error::EditorInvalidated)
    }

    fn open_cfurls(&self, item_urls: CFArray<CFURL>) -> Result<(), Error> {
        let app_url = self.bundle_url()?;
        let status = unsafe {
            LSOpenFromURLSpec(&LSLaunchURLSpec {
                appURL: app_url.as_concrete_TypeRef(),
                itemURLs: item_urls.as_concrete_TypeRef(),
                launchFlags: kLSLaunchAndDisplayErrors | kLSLaunchDontAddToRecents,
                asyncRefCon: std::ptr::null_mut(),
                passThruParams: std::ptr::null_mut(),

            }, std::ptr::null_mut())
        };
        if status == 0 {
            Ok(())
        } else {
            Err(Error::Status(status as _))
        }
    }
}

pub struct Sublime {
    subl_path: PathBuf,
    fallback: EditorBundle,
}

impl EditorImpl for Sublime {
    fn open_paths<'a>(&self, paths: &[&Path]) -> Result<(), Error> {
        if self.launch(paths) {
            Ok(())
        } else {
            self.fallback.open_paths(paths)
        }
    }

    fn name(&self) -> Cow<str> {
        self.fallback.name()
    }
}

impl Sublime {
    pub fn subl_path(ed: &EditorBundle) -> Option<PathBuf> {
        let ed_path = PathBuf::from(ed.bundle_url().ok()?.get_file_system_path(kCFURLPOSIXPathStyle).to_string());
        let subl_path = ed_path.join("Contents/SharedSupport/bin/subl");
        if subl_path.exists() {
            Some(subl_path)
        } else {
            None
        }
    }

    fn launch(&self, paths: &[&Path]) -> bool {
        let mut cmd = std::process::Command::new(&self.subl_path);
        let same_window = paths.len() == 1 && paths[0].is_file();
        if !same_window {
            cmd.arg("-n");
        }
        match cmd.args(paths).status() {
            Ok(s) if s.success() => true,
            _ => false,
        }
    }
}

pub struct Xcode {
    inner: EditorBundle,
}

impl EditorImpl for Xcode {
    fn open_paths(&self, paths: &[&Path]) -> Result<(), Error> {
        let last_path = if let Some(p) = paths.last() {p} else {
            return Ok(());
        };

        // FB7693737: Xcode fails to open a dir. There must be a sacrificial file at the end.
        let mut tmp;
        let paths = if !last_path.is_file() {
            tmp = paths.to_vec();
            if let Some((file_idx, _)) = tmp.iter().enumerate().rev().find(|(_, p)| p.is_file()) {
                let file = tmp.remove(file_idx);
                tmp.push(file);
            } else {
                let xcode_bug_workaround = Path::new("/tmp/ ");
                std::fs::write(xcode_bug_workaround, "")?;
                tmp.push(xcode_bug_workaround);
            }
            &tmp
        } else {
            paths
        };
        self.inner.open_paths(paths)
    }

    fn name(&self) -> Cow<str> {
        self.inner.name()
    }
}

/// Find what is the default editor for files of a given type
pub fn editor_for_file_extension(ext: &str) -> Option<Editor> {
    let (bundle, bundle_id) = services::cfbundle_for_file_extension(ext)?;
    let ed = EditorBundle::new(bundle);
    let inner: Arc<dyn EditorImpl> = if let Some(subl_path) = Sublime::subl_path(&ed) {
        Arc::new(Sublime { subl_path, fallback: ed })
    } else {
        match bundle_id.as_str() {
            "com.apple.dt.Xcode" => Arc::new(Xcode { inner: ed }),
            _ => Arc::new(ed),
        }
    };
    Some(Editor { inner })
}

/// Reveal paths in the system default file manager (e.g. macOS Finder)
///
/// This call is non-blocking
pub fn reveal_paths(paths: &[&Path]) -> Result<(), Error> {
    if appkit::reveal_paths(array_of_paths(paths)?.as_concrete_TypeRef()) {
        Ok(())
    } else {
        Err(Error::WorkspaceUnavailable)
    }
}

fn array_of_paths(paths: &[&Path]) -> Result<CFArray<CFURL>, Error> {
    let paths = paths.iter().map(std::fs::canonicalize).collect::<Result<Vec<_>, _>>()?;
    let paths = paths.into_iter().map(|p| CFURL::from_path(p, false))
        .collect::<Option<Vec<CFURL>>>().ok_or(Error::InvalidPaths)?;
    Ok(CFArray::from_CFTypes(&paths))
}

#[test]
fn spawn_m() {
    let editor = editor_for_file_extension("m").unwrap();
    editor.open_paths(["Cargo.toml", "src"]).unwrap();
}

#[test]
fn cocoa() {
    let uti = uti_for_extension("js").expect("uti");
    assert_eq!("com.netscape.javascript-source", uti.to_string());
    let bundle_id = default_handler_bundle_identifier_for_uti(uti.as_concrete_TypeRef(), kLSRolesEditor | kLSRolesViewer).expect("role");
    let urls = urls_for_bundle_identifier(bundle_id.as_concrete_TypeRef());
    assert!(!urls.is_empty());
    let bundle = CFBundle::new(urls[0].clone()).expect("bundle");
    let info = bundle.info_dictionary();
    let name = info.get(CFString::new("CFBundleName"));
    assert!(!name.downcast::<CFString>().unwrap().to_string().is_empty());
}
